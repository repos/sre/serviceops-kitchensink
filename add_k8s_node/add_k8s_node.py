#!/usr/bin/env python3

import argparse
import re
import subprocess
import sys
import urllib.parse

from collections import deque
from contextlib import contextmanager
from operator import itemgetter
from pathlib import Path
from typing import Optional, Union

import pynetbox
import requests
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedSeq, CommentedMap

# These rows need their top of rack switch updated, otherwise update the respective core router
NETBOX_TOR_ROWS = ["eqiad-row-e", "eqiad-row-f", "codfw-row-a", "codfw-row-b", "codfw-row-c", "codfw-row-d"]


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--puppet-dir",
        type=lambda p: Path(p).absolute(),
        help="Path to the puppet git repo",
    )
    parser.add_argument(
        "--netbox-url",
        type=str,
        default="https://netbox.wikimedia.org/",
        help="Netbox URL, use netbox.discovery.wmnet when running from an internal host",
    )
    parser.add_argument(
        "--netbox-token",
        type=str,
        help="Token to access Netbox -- create at https://netbox.wikimedia.org/user/api-tokens/",
    )
    parser.add_argument(
        "--task-id",
        type=str,
        default="T351074",
        help="Phabricator task ID to update with changes",
    )
    parser.add_argument(
        "--netbox-commit",
        action="store_true",
        help="Run without this to generate Puppet patch, re-run with this to instead make live changes in Netbox",
    )
    parser.add_argument(
        "--rename-only",
        action="store_true",
        help="Only rename the nodes",
    )
    parser.add_argument(
        "--move-vlan",
        action="store_true",
        help="Also perform vlan move and ip renumbering",
    )
    parser.add_argument("fqdns", nargs="+", help="FQDNs of the nodes to add")
    return parser.parse_args()


class ToDoOutputter:
    """An object to format todo output and track the number."""

    def __init__(self):
        """Create a ToDoOutputter"""

        self.todo_idx = 1

    def todo(self, command, message) -> str:
        """Format and print a todo with the current index"""
        message = f"{self.todo_idx}.) {bold(command)} {message}"
        self.todo_idx += 1
        return message


def bold(s):
    return f"\033[1m{s}\033[0m"


def init_ruamel():
    yaml_init = YAML(typ="rt")
    # Allow for mixed quotes and quotes around strings which are not required
    yaml_init.preserve_quotes = True
    # Indent lists
    yaml_init.indent(mapping=2, sequence=4, offset=2)
    # Break lines after 4096 characters
    yaml_init.width = 4096
    return yaml_init


@contextmanager
def kubernetes_yaml(path):
    """
    Load the kubernetes.yaml file and yield the data.
    """
    k8s_yaml = init_ruamel()

    # Ensure nil/null is represented as ~
    # this is used in kubernetes.yaml
    def represent_none_as_tilde(self, data):
        return self.represent_scalar("tag:yaml.org,2002:null", "~")

    k8s_yaml.representer.add_representer(type(None), represent_none_as_tilde)
    k8s_yaml_data = k8s_yaml.load(path)
    try:
        yield k8s_yaml_data
    finally:
        k8s_yaml.dump(k8s_yaml_data, path)


@contextmanager
def puppet_yaml(path):
    """
    Load the puppet conftool-data/node/$dc.yaml file and yield the data.
    """
    pup_yaml = init_ruamel()
    pup_yaml_data = pup_yaml.load(path)
    try:
        yield pup_yaml_data
    finally:
        pup_yaml.dump(pup_yaml_data, path)


def netbox_commit(fqdns, api_url, token):
    """
    Sets BGP to true for the given hosts in Netbox.

    Returns the switches that need a Homer update.
    """

    switches_to_update = set()

    nb = pynetbox.api(api_url, token=token)
    for fqdn in fqdns:
        name = fqdn.split(".")[0]
        dev = nb.dcim.devices.get(name=name)
        print(f"[{dev}] BGP: {dev.custom_fields['bgp']} -> True")
        dev.custom_fields["bgp"] = True
        dev.save()

        if dev.location.slug in NETBOX_TOR_ROWS:
            for interface in nb.dcim.interfaces.filter(device_id=dev.id):
                if interface._occupied:
                    for connected_endpoint in interface.connected_endpoints:
                        if connected_endpoint._occupied:
                            switches_to_update.add(f"{connected_endpoint.device.name}*")
        else:
            switches_to_update.add(f"cr*{dev.site.slug}*")

    return switches_to_update


def original_hostname_to_workers(fqdns, dc, api_url, token):
    """
    For a list of original hostnames, rename them to wikikube-workerNNNN, skipping any that already exist in Netbox
    Return a dict with the original hostnames as keys and the new hostnames as values
    """

    # Get all in use worker node ids, including decommissioned ones from netbox GraphQL
    device_list_gql = """
    query ($name: String) {
        device_list(filters: {name: { i_contains: $name }}) {
            name
        }
    }
    """

    if dc == "eqiad":
        worker_prefix = "wikikube-worker1"
    elif dc == "codfw":
        worker_prefix = "wikikube-worker2"
    else:
        raise ValueError(f"Unknown datacenter: {dc}")

    workers = [
        i["name"]
        for i in _gql_execute(
            url=api_url, token=token, query=device_list_gql, variables={"name": worker_prefix}
        )["device_list"]
    ]

    worker_node_ids = [int(re.match("wikikube-worker(\d\d\d\d)", i).groups()[0]) for i in sorted(workers)]

    highest_id = max(worker_node_ids)
    # Find gaps in the sequence
    all_possible_ids = set(range(min(worker_node_ids), highest_id + 1))
    missing_ids = deque(sorted(all_possible_ids - set(worker_node_ids)))

    renamed_names = {}
    for fqdn in fqdns:
        if str(fqdn).startswith("wikikube-worker"):
            renamed_names[fqdn] = fqdn
        else:
            if missing_ids:
                next_worker_id = missing_ids.popleft()
            else:
                next_worker_id = highest_id + 1
                highest_id += 1
            renamed_names[fqdn] = f"wikikube-worker{next_worker_id}.{dc}.wmnet"
    return renamed_names


def git_cmd(puppet_dir, command_args) -> str:
    """
    Run a git command in the given directory and return the output.
    """

    git_c = subprocess.Popen(["git", "-C", puppet_dir] + command_args, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    stdout, stderr = git_c.communicate()

    if git_c.returncode:
        raise RuntimeError(f'Failed to run git command "{" ".join(command_args)}": {stdout!r} ({stderr!r})')
    return stdout.decode("utf-8")


def _gql_execute(url: str, token: str, query: str, variables: Optional[dict] = None) -> dict:
    """Parse the query into a gql query, execute and return the results.
    Arguments:
        query: a string representing the gql query
        variables: A list of variables to send
    Results:
        dict: the results
    """
    data: dict[str, Union[str, dict]] = {"query": query}
    if variables is not None:
        data["variables"] = variables
    session = requests.Session()
    session.headers.update({"Authorization": f"Token {token}", "User-Agent": "add_k8s_node.py"})
    response = session.post(f"{url}/graphql/", json=data, timeout=15)
    response.raise_for_status()
    return response.json()["data"]


def puppet_current(puppet_dir) -> bool:
    """
    Check if the puppet directory is up to date.
    """

    git_fetch = ["fetch", "origin"]
    git_status = ["status", "-sb"]

    git_cmd(puppet_dir, git_fetch)
    status_out = git_cmd(puppet_dir, git_status)

    if "behind" in str(status_out):
        return False
    return True


if __name__ == "__main__":
    args = parse_arguments()
    yaml = init_ruamel()

    args.fqdns = sorted(args.fqdns)

    fqdns_by_dc: dict[str, list[str]] = {}
    for fqdn in args.fqdns:
        for dc in ["eqiad", "codfw"]:
            if dc in fqdn:
                fqdns_by_dc.setdefault(dc, []).append(fqdn)
                break

    if args.netbox_commit:
        homer_targets = netbox_commit(args.fqdns, api_url=args.netbox_url, token=args.netbox_token)
        print("####### TODOs #######")
        print(f"1.) {bold('Run homer')}:")
        for target in homer_targets:
            print(f"homer '{target}' commit '{args.task_id}'")
        sys.exit(0)

    if not args.puppet_dir:
        raise ValueError("--puppet-dir is a required argument when not using --netbox-commit")

    if not puppet_current(args.puppet_dir):
        print("Puppet directory is not up to date! `git pull` your puppet dir to ensure no clashing changes")
        sys.exit(1)

    # The wikikube-worker hostnames nodes are being renamed to
    new_hostnames = {}

    for dc in ["eqiad", "codfw"]:
        if dc in fqdns_by_dc:
            # Add nodes as kubernetes workers
            with kubernetes_yaml(args.puppet_dir / "hieradata/common/kubernetes.yaml") as yd:
                new_hostnames.update(original_hostname_to_workers(
                    fqdns_by_dc[dc],
                    dc,
                    args.netbox_url,
                    args.netbox_token,
                ))
                if args.rename_only:
                    for node in fqdns_by_dc[dc]:
                        try:
                            yd["kubernetes::clusters"]["main"][dc]["cluster_nodes"].remove(node)
                        except KeyError:
                            pass
                yd["kubernetes::clusters"]["main"][dc]["cluster_nodes"].extend(new_hostnames.values())
                yd["kubernetes::clusters"]["main"][dc]["cluster_nodes"] = CommentedSeq(sorted(
                    list(set(yd["kubernetes::clusters"]["main"][dc]["cluster_nodes"]))
                ))

                if dc == "eqiad":
                    if sorted(new_hostnames.values())[-1] != "wikikube-worker1239.eqiad.wmnet":
                        # Keep the blank space comment intact
                        comment_idx = yd["kubernetes::clusters"]["main"][dc]["cluster_nodes"].index("wikikube-worker1240.eqiad.wmnet")
                        yd["kubernetes::clusters"]["main"][dc]["cluster_nodes"].yaml_set_comment_before_after_key(
                            comment_idx,
                            before="This space intentionally left blank - new hosts start at 1240 (T369743), below reserved for renames",
                            indent=8)
                    # Keep comment about wikikube-worker1290 intact
                    comment_idx = yd["kubernetes::clusters"]["main"][dc]["cluster_nodes"].index("wikikube-worker1291.eqiad.wmnet")
                    yd["kubernetes::clusters"]["main"][dc]["cluster_nodes"].yaml_set_comment_before_after_key(
                        comment_idx,
                        before="wikikube-worker1290 reimaged as wikikube-ctrl1004 T379790",
                        indent=8)

            with puppet_yaml(args.puppet_dir / f"conftool-data/node/{dc}.yaml") as yd:
                for node in fqdns_by_dc[dc]:
                    # Remove nodes from appserver/api_appserver cluster or from kubernetes cluster
                    if args.rename_only:
                        del yd[dc]["kubernetes"][node]
                    else:
                        for cluster in ["appserver", "api_appserver"]:
                            try:
                                del yd[dc][cluster][node]
                            except KeyError:
                                pass

                for new_hostname in new_hostnames.values():
                    # Add nodes to kubernetes cluster (kubesvc)
                    yd[dc]["kubernetes"][new_hostname] = CommentedSeq(
                        [
                            "kubesvc",
                        ]
                    )
                    yd[dc]["kubernetes"][new_hostname].fa.set_flow_style()
                yd[dc]["kubernetes"] = CommentedMap(dict(sorted(yd[dc]["kubernetes"].items(), key=itemgetter(1, 0))))

                if dc == "eqiad":
                    if sorted(new_hostnames.values())[-1] != "wikikube-worker1239.eqiad.wmnet":
                        # Keep the blank space comment intact
                        yd[dc]["kubernetes"].yaml_set_comment_before_after_key(
                            "wikikube-worker1240.eqiad.wmnet",
                            before="This space intentionally left blank - new hosts start at 1240 (T369743), below reserved for renames",
                            indent=4)
                    # Keep comment about wikikube-worker1290 intact
                    yd[dc]["kubernetes"].yaml_set_comment_before_after_key(
                        "wikikube-worker1291.eqiad.wmnet",
                        before="wikikube-worker1290 reimaged as wikikube-ctrl1004 T379790",
                        indent=4)

            # Remove host-specific hieradata yaml
            for node in fqdns_by_dc[dc]:
                host_hiera_path = args.puppet_dir / f"hieradata/hosts/{node.split('.')[0]}.yaml"
                try:
                    host_hiera_path.unlink()
                except FileNotFoundError:
                    pass
    renames_present = any(not fqdn.startswith("wikikube-worker") for fqdn in args.fqdns)

    print("####### TODOs #######")
    todo_output = ToDoOutputter()
    if renames_present:
        print(todo_output.todo("Depool", " hosts:"))
        if args.rename_only:
            for dc in ["eqiad", "codfw"]:
                if dc in fqdns_by_dc:
                    print(
                        f"sudo cookbook sre.k8s.pool-depool-node --k8s-cluster wikikube-{dc} "
                        f"-t {args.task_id} -r 'Renaming nodes' depool {','.join(fqdns_by_dc[dc])}"
                    )
        else:
            print(f"sudo cumin -m async '{','.join(args.fqdns)}' 'decommission; sleep 120'")

    print(
        todo_output.todo(
            "Extend the hostname globs",
            (
                f" as appropriate in {bold(args.puppet_dir / 'manifests/site.pp')}."
                " Remove the entries for the pre-rename hostnames."
            ),
        )
    )

    if args.rename_only:
        print(
            todo_output.todo(
                "Remove the entries for the pre-rename or insetup::serviceops hostnames",
                f"in {bold(args.puppet_dir / 'modules/profile/data/profile/installserver/preseed.yaml')}",
            )
        )

    print(todo_output.todo("Verify and commit", "changes to puppet repo, review, merge etc."))

    if args.rename_only:
        print(todo_output.todo("Delete", "the nodes from k8s api:"))
        for dc in ["eqiad", "codfw"]:
            if dc in fqdns_by_dc:
                print(f"kube-env admin {dc}")
                print(f"kubectl delete node {' '.join(fqdns_by_dc[dc])}")

    if renames_present:
        print(todo_output.todo("Run the rename", "cookbook:"))
        for oldname in args.fqdns:
            if not oldname.startswith("wikikube-worker"):
                oldname_short = oldname.split(".", 1)[0]
                new_name_short = new_hostnames[oldname].split(".", 1)[0]
                print(f"sudo cookbook sre.hosts.rename -t {args.task_id} {oldname_short} {new_name_short}")
        if args.rename_only:
            print(todo_output.todo("Run puppet", f"on registry {bold('after finishing your renames')}:"))
            print("sudo cumin 'A:docker-registry' 'run-puppet-agent'")

    if args.move_vlan:
        print(todo_output.todo("Run the renumber", "cookbook:"))
        for fqdn in new_hostnames.values():
            print(f"sudo cookbook sre.k8s.renumber-node -t {args.task_id} -R {fqdn}")
        print(
            "It will reimage, renumber, update netbox, prompt for homer runs, and rejoin the nodes to the k8s cluster"
        )
    else:
        print(todo_output.todo("Run the reimage", "cookbook:"))
        for node in [fqdn.split(".", 1)[0] for fqdn in new_hostnames.values()]:
            print(
                f"sudo cookbook sre.hosts.reimage --puppet 7 --new -t {args.task_id} "
                f"--os bookworm --move-vlan --force {node}"
            )
    if renames_present:
        print(
            "Note: If the rename cookbook just completed, this may fail with a DNS resolution error. "
            "This can be fixed by flushing the DNS cache:"
        )
        print(f"sudo cookbook sre.dns.wipe-cache {' '.join(new_hostnames.values())}")

    if not args.move_vlan:
        print(
            todo_output.todo(
                "Update Netbox'", "(remember to run homer afterwards and !log your action on #wikimedia-operations):"
            )
        )
        print(
            f"{sys.argv[0]} --netbox-token $NETBOX_TOKEN --netbox-commit "
            f"--task-id {args.task_id} {' '.join(new_hostnames.values())}"
        )

        print(todo_output.todo("Pool", "the new nodes:"))
        for dc in ["eqiad", "codfw"]:
            hosts = ",".join(filter(lambda h: dc in h, new_hostnames.values()))
            if hosts:
                print(
                    f"sudo cookbook sre.k8s.pool-depool-node --k8s-cluster wikikube-{dc} "
                    f"-t {args.task_id} pool {hosts}"
                )

    if renames_present:
        print(todo_output.todo("Create a relabel task", "in Phabricator if one does not exist yet"))
        for dc in ["eqiad", "codfw"]:
            if dc in fqdns_by_dc:
                task_url = (
                    f"https://phabricator.wikimedia.org/maniphest/task/edit/form/1/?"
                    f"parent={args.task_id[1:]}&"
                    f"projectPHIDs=serviceops,DC-ops,Prod-Kubernetes,Kubernetes,ops-{dc}&"
                    f"priority=low&status=open&"
                    f"title=Relabel%20{dc}%20kubernetes%20nodes&description="
                )
                task_description = "Please relabel the following nodes following their rename:\n"
                for oldname in fqdns_by_dc[dc]:
                    if not oldname.startswith("wikikube-worker"):
                        oldname_short = oldname.split(".", 1)[0]
                        new_name_short = new_hostnames[oldname].split(".", 1)[0]
                        task_description += f"[ ] `{oldname_short}` to `{new_name_short}`\n"
                task_description += "Thanks!"
                print(f"{task_url}{urllib.parse.quote(task_description)}")
