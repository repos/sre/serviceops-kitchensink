# `add_k8s_node`: move MW servers to k8s

## Usage

1. Run this script with `--puppet-dir ...` to generate a starting point for the Puppet patch.
   This also prints out detailed instructions.
2. After the reimage step is complete, you will need to re-run this script with `--netbox-commit` as
   mentioned in the instructions. This sets up the Netbox changes required for k8s networking. You
   will also need to pass `--netbox-token`, create one at
   https://netbox.wikimedia.org/user/api-tokens/ (write permissions are needed; ideally set an
   expiry time).
